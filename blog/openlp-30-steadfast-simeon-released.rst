.. title: OpenLP 3.0 "Steadfast Simeon" Released
.. slug: 2022/12/31/openlp-30-steadfast-simeon-released
.. date: 2022-12-31 12:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-30-steadfast-simeon-released.jpg

Merry Christmas and Happy New Year, it's finally here! Yes, OpenLP 3.0 has been released!


    And whatever you do, work heartily, as for the Lord, and not for men, knowing that from the
    Lord you will receive the reward of the inheritance; for you serve the Lord Christ.

    Colossians 3:23-24 WEB


One of our original goals with OpenLP was to move to faster releases, and while that didn't seem to
happen for version 3.0, moving foward we are aiming to roll out releases much faster. With that in
mind, we decided that the worst bugs have been fixed, and most people should be pretty happy with
the 3.0 release.

Known Issues
------------

Before we dive into the new features, here are some known issues which may apply to you:

.. warning:: **Ubuntu 22.04**

   The main view in the web remote does not update automatically on Ubuntu 22.04 due to Ubuntu
   shipping an old version of python3-websockets. This can be fixed by downloading and installing
   an updated version of the package from `the official Ubuntu archive`_.

.. warning:: **Wayland on Linux**

   OpenLP at present does not behave well under Wayland so the recommendation is to run under X11.
   If you can't run in X11 (or prefer to run it in XWayland), you should start it with
   ``QT_QPA_PLATFORM`` environment variable set to ``xcb``, although the Main View from the Web
   Remote will not work.
 
.. warning:: **Video backgrounds**

   Video / streaming backgrounds and background audio in songs currently do not work together. You
   may experience either the video not working but the audio works, or the audio doesn't work while
   the video does.

.. warning:: **macOS**

   macOS may report OpenLP as damaged. If you encounter this problem, you can try the following
   solution:

   1. From Applications / Utilities run Terminal
   2. In Terminal, type in the following command::

        xattr -dr com.apple.quarantine /Applications/OpenLP.app

   3. This should allow you to start OpenLP normally

New Features
------------

One of the biggest changes in version 3.0 is that we completely rewrote the renderer, meaning that
we were able to bring in efficient transitions between slides for text items (i.e. songs, Bible
verses, custom slides). Transitions are available as part of themes.

Another major change in version 3.0 is the remote. Maintaining separate remotes for browsers,
Android and iOS was painful and slow. In version 3.0 we rewrote the remote API, and have
consolidated our work into the web remote. The web remote is also no longer bundled with OpenLP,
allowing us to roll out updates to the remote separately from OpenLP itself. The old API's will be
removed in a future release breaking the Android and IOS applications. Please migrate to the new
Web Interface.

In addition to the two above, OpenLP now supports presentations on macOS, using LibreOffice,
Microsoft PowerPoint for Mac and Keynote. Support of Microsoft Powerpoint Viewer has been removed.

Other New Features
------------------

- VLC is now bundled with OpenLP on macOS and Windows, and there is no need to select a media
  players
- Guide marks to show position within a clip and the ability to repeat the playing video
- Motion (video) backgrounds in themes
- Better screen detection and configuration
- Performance improvements reducing start up times
- The settings page has been updated, so a number of options have moved
- All icons have been migrated from images to scalable monochromatic fonts to allow for the
  support of larger desktop displays and dark themes
- OpenLP now ships with a dark interface/UI theme. On Windows and macOS, it can be enabled by
  enabling the dark theme on system. You can always force the dark theme by changing the Interface
  Theme in Settings
- Media and Presentations now support being grouped by folders
- Slide numbers in footer can be enabled in Settings
- Songs can be searched by number
- Chord support in songs (can be enabled in Song settings). The chords are shown in the "Chords
  View" section of Web Remote, which supports chord transposition and mobile devices
- More song formats can be imported: 
  - Singing The Faith
  - LivePresenter
  - ProPresenter 5 and 6
  - EasyWorship 6
  - ChordPro (for chords)
- More bible formats can be imported:
  - SWORD
  - WordProject
- Support for importing services from PlanningCenter

Updates and Bug Fixes
---------------------

A lot more has been fixed, updated or improved, far too much to include here. If you're really
curious, you can do a `comparison between 2.4.6 and 3.0.0`_ on GitLab.

Download
--------

Head on over to the `downloads section of the website`_ to download version 3.0 now!

.. raw:: html

   <p style="text-align: center"><a href="https://openlp.org/#downloads" class="btn btn-primary">Download</a></p>


.. _the official Ubuntu archive: http://archive.ubuntu.com/ubuntu/pool/universe/p/python-websockets/python3-websockets_10.2-1_all.deb
.. _comparison between 2.4.6 and 3.0.0: https://gitlab.com/openlp/openlp/-/compare/3.0.0...2.4.6
.. _downloads section of the website: https://openlp.org/#downloads
