.. title: Thank You for Your Donations!
.. slug: blog/2023/06/07/thank-you-for-your-donations
.. date: 2023-06-07 12:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/thank-you-for-your-donations.jpg

**Thank you so much!**

We want to say a **HUGE** thank you to our community for the recent donations we have received. The
recent donations we have received have enabled us to purchase the following items:

High DPI monitor
----------------

Many people have had issues on Windows with high DPI monitors and scaling. Thanks to the community,
we were able to purchase a 4K monitor so that we can test OpenLP with high resolutions and hopefully
narrow down these issues.

Apple Silicon Mac Mini
----------------------

Due to a lack of resources, up until now we have not been able to build OpenLP on the new Apple
Silicon platform, resulting in various compatibility issues, especially with video. Thanks to some
extremely generous members of our community, we were able to purchase a Mac Mini M2, and we'll be
using that for building Apple Silicon versions of OpenLP, as well as testing and development of
OpenLP on that platform.

Extra RAM
---------

We are also using virtual machines for running some tests and some development on Windows. These can
be quite resource intensive, and my desktop computer was not able to fully handle it. Thanks to our
community, the cost of an extra 16GB of RAM was covered, and we're able to run more virtual machines
for development and testing.

Ongoing Expenses
----------------

We do still have our regular ongoing expenses (domain names, downloads and forums hosting, etc), and
we are really grateful for the continued financial support of the community. Thank you!
