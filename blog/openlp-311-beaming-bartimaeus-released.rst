.. title: OpenLP 3.1.1 "Beaming Bartimaeus" Released
.. slug: 2024/03/10/openlp-311-beaming-bartimaeus-released
.. date: 2024-03-10 12:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-311-beaming-bartimaeus-released.jpg

Hot on the heals of the 3.1 release is 3.1.1 which fixes a couple of bugs that some people have encountered with the 3.1 release.

We also have an experimental Flatpak build for those on Linux who are feeling adventurous.

   So then the Lord, after he had spoken to them, was received up into heaven and sat down at the right hand of God.
   They went out and preached everywhere, the Lord working with them and confirming the word by the signs that
   followed. Amen.

   Mark 16:19-20

New Features
------------

* Experimental Flatpak build

Bugs Fixed
----------

* Fix path to QtWebEngineProcess binary in macOS builds
* Use Python's version comparison, not Qt's to align with new versioning scheme
* Always open downloaded songs as utf-8 when downloading from Song Select
* Update and fix a number of translations

Known Issues
------------

See the `OpenLP 3.1 release blog post </blog/2024/02/29/openlp-310-superb-seth-released>`_ for the list of known issues.

Download
--------

Download the latest version of OpenLP from the downloads section on the site.
