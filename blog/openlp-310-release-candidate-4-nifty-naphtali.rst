.. title: OpenLP 3.1.0 Release Candidate 4 "Nifty Naphtali"
.. slug: 2023/12/18/openlp-310-release-candidate-4-nifty-naphtali
.. date: 2023-12-18 20:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-310-release-candidate-4-nifty-naphtali.jpg

We're happy to announce the fourth release candidate of OpenLP 3.1. This release fixes or improves some of the biggest bugs and issues we've seen in OpenLP. This is the last release candidate before the final release of OpenLP 3.1.

   What then is my reward? That when I preach the Good News, I may present the Good News of Christ without charge, so as not to abuse my authority in the Good News.

   1 Corinthians 9:18

.. note:: **Windows 7 is no longer supported!** (Microsoft does not support it either.)

   In order to make use of newer technologies, features and optimizations in the libraries that OpenLP uses, we have
   had to drop support for Windows 7. Microsoft stopped supporting Windows 7 in January 2020, and Python stopped
   supporting Windows 7 with the release of Python 3.9 in October 2020.

.. warning:: **Download on macOS is damaged**

   macOS reports OpenLP as damaged, due to the OpenLP app not being signed. If you encounter this
   problem, you can try the following solution:

   1. From Applications / Utilities run Terminal
   2. In Terminal, type in the following command::
           
       xattr -dr com.apple.quarantine /Applications/OpenLP.app
           
   3. In some cases, you may need to prepend ``sudo`` to the above command
   4. This should allow you to start OpenLP normally

.. warning:: **Video/media playback on macOS**

   OpenLP uses VLC for video and media playback. At this stage VLC is not bundled with OpenLP, so you will need to
   download and install it yourself.

   You can download VLC from the `VLC website`_.

.. warning:: **Ubuntu 22.04, Linux Mint**

   The main view in the web remote does not update automatically on Ubuntu 22.04 due to
   Ubuntu/Mint shipping an old version of python3-websockets. This can be fixed by
   downloading and installing an updated version of the package from `the official
   Ubuntu archive`_.

.. warning:: **Wayland on Linux**

   OpenLP at present does not behave well under Wayland so the recommendation is to run
   under X11. If you can't run in X11 (or prefer to run it in XWayland), you should
   start it with ``QT_QPA_PLATFORM`` environment variable set to ``xcb``, although the
   Main View from the Web Remote will not work.

.. warning:: **Video backgrounds**

   Video / streaming backgrounds and background audio in songs currently do not work
   together. You may experience either the video not working but the audio works, or
   the audio doesn't work while the video does.


Download
--------

.. note:: **Debian/Ubuntu builds will be coming soon**

   We will update this blog post once the Debian/Ubuntu package has been built

.. raw:: html

   <div class="text-center" style="margin-bottom: 2em">
    <div class="btn-group">
      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-windows"></i>
        Windows
        &nbsp;
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="https://get.openlp.org/3.1.0rc4/OpenLP-3.1.0rc4-x64.msi">64-bit Installer</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc4/OpenLPPortable_3.1.0.3004-x64.paf.exe">64-bit Portable</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc4/OpenLP-3.1.0rc4.msi">32-bit Installer</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc4/OpenLPPortable_3.1.0.3004-x86.paf.exe">32-bit Portable</a></li>
      </ul>
    </div>
    <div class="btn-group">
      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-apple"></i>
        macOS
        &nbsp;
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="https://get.openlp.org/3.1.0rc4/OpenLP-3.1.0rc4-x86_64.dmg">For Macs with Intel Processors</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc4/OpenLP-3.1.0rc4-arm64.dmg">For Macs with Apple Processors</a></li>
      </ul>
    </div>
    <!-- a class="btn btn-ubuntu" href="https://get.openlp.org/3.1.0rc4/openlp_3.1.0rc4-1_all.deb">
      <img class="icon notranslate" src="/images/ubuntu-logo.png"/> Ubuntu
    </a>
    <a class="btn btn-debian" href="https://get.openlp.org/3.1.0rc4/openlp_3.1.0rc4-1_all.deb">
      <img class="icon notranslate" src="/images/debian-logo.png"/> Debian
    </a -->
    <a class="btn btn-fedora" href="https://copr.fedorainfracloud.org/coprs/trb143/OpenLP/">
      <img class="icon notranslate" src="/images/fedora-logo.png"/> Fedora
    </a>
    <a class="btn btn-success" href="https://get.openlp.org/3.1.0rc4/OpenLP-3.1.0rc4.tar.gz">
      <i class="fa fa-file-archive-o"></i> Source
    </a>
   </div>

Fixes Since Release Candidate 2
-------------------------------

* Fix irregular service theme saving (closes #1723) 
* Fix AuthorType not getting translated 
* Fix bug in _has_header 
* Fix issues with upgrading 2.9.x databases 
* High DPI fixes 
* Fix error when no Bible is available while importing from PlanningCenterOnline
* Check before initialising a None Bible 
* Fix #1700 by typecasting the calls to Paths 
* Make PathEdit handle None values 
* Fix external DB settings 
* Fix alerts 
* Fixes #1325 - Handling of Missing VLC. 
* Better handling of attempts to load invalid SWORD folder or zip-file 
* Ensure a path set in PathEdit is a Path instance 
* Fix OpenLyrics import to trim leading whitespace in verses
* Fix SongSelect browsing by implementing missing JavaScript APIs
* Do not start the same presentation again when it's already live
* Prevent key error when unblank screen at start of presentation
* Fix the startup order to prevent issues in Windows and Portable builds


.. _the official Ubuntu archive: http://archive.ubuntu.com/ubuntu/pool/universe/p/python-websockets/python3-websockets_10.2-1_all.deb
.. _VLC website: https://www.videolan.org/vlc/download-macosx.html
