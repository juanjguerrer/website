.. title: Donate to OpenLP
.. slug: donate
.. date: 2015-10-03 20:31:32 UTC
.. type: text
.. hidetitle: True
.. previewimage: /cover-images/donate.jpg

.. raw:: html

   <div class="space"></div>

Like most other voluntary open source projects, OpenLP has no income and is entirely funded by the personal funds of
the developers. We have a number of fixed costs, like server hosting, domain names, and the like. If you'd like to
help us out, please consider donating to our cause.

.. raw:: html

    <div id="error-alert" class="alert alert-danger hidden" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" tx-attrs="aria-label"><span class="notranslate" aria-hidden="true">&times;</span></button>
      <p><strong>Error:</strong> <span id="error-message"></span></p>
    </div>
    <div id="success-alert" class="alert alert-success hidden" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" tx-attrs="aria-label"><span class="notranslate" aria-hidden="true">&times;</span></button>
      <p id="success-message"></p>
    </div>
    <div class="clearfix">
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">  
        <h3>PayPal <small>(Preferred)</small></h3>
        <p>You can donate via PayPal by clicking the button.</p>
        <p><a class="btn btn-primary" href="https://paypal.me/raoulsnyman"><i class="fa fa-fw fa-paypal"></i> Donate via PayPal</a></p>
        <p class="small text-info">You will be guided to create a PayPal account if you don't have one already.</p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">  
        <h3>Bitcoin</h3>
        <p>Donate to us via Bitcoin using the address below.</p>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-fw fa-btc"></i></span>
          <input class="form-control type="text" value="bc1qzmlan4lwwexr4fhevq972ugnganr8c9skgg02s" readonly>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">  
        <h3>Credit Card</h3>
        <p>We also accept donations via debit or credit card.</p>
        <script async src="https://js.stripe.com/v3/buy-button.js"></script>
        <stripe-buy-button buy-button-id="buy_btn_1MjEIzF4wrjjReq2DT6uSRgi" publishable-key="pk_live_GWIWaLW8W53LNTthomxAyO5w"></stripe-buy-button>
      </div>
    </div>
    <hr>


Current Needs
^^^^^^^^^^^^^

The reality is that most of the donated money we receive is contributed toward various server costs,
as detailed below. From time to time, however, we have additional needs that are usually financed
by the OpenLP team themselves. Below are some of those needs, and whether or not we have purchased
them already.

.. table::
   :class: table

   +------------------------------------------------------------------+-------------------+-------------------+---------------+----------------------+
   | Item                                                             | Cost              | Covered           | Purchased     |       Keywords       |
   +==================================================================+===================+===================+===============+======================+
   | |ss| `Extra RAM for virtual machines`_ |se|                      | |ss| $39.99 |se|  | |ss| $39.99 |se|  | |ss| Yes |se| | |ss| RAM |se|        |
   +------------------------------------------------------------------+-------------------+-------------------+---------------+----------------------+
   | |ss| `4K monitor for Hi DPI testing`_ |se|                       | |ss| $169.99 |se| | |ss| $169.99 |se| | |ss| Yes |se| | |ss| 4K Monitor |se| |
   +------------------------------------------------------------------+-------------------+-------------------+---------------+----------------------+
   | |ss| `Apple M2 Mac Mini`_ |se|                                   | |ss| $599.00 |se| | |se| $599.00 |se| | |ss| Yes |se| | |ss| M2 Mac |se|     |
   +------------------------------------------------------------------+-------------------+-------------------+---------------+----------------------+


If you feel led to give for something specifically, please feel free to use the keywords when you
donate. Below are ways to get hold of us:

* If you use PayPal, add the keywords in the message
* If you use your credit card, add the keywords in the keywords field
* You can direct message `Raoul`_ via `your Inbox`_ on the `forums`_
* You can `come into IRC`_ and let "superfly", "TRB143" or "tgc" know your amount and keywords

.. raw:: html

   <hr>

Regular Costs
^^^^^^^^^^^^^
Here's a breakdown of our regular costs:

1. $40/month for a `VPS from Linode.com`_ which is used for downloads, developer tools and First Time Wizard data.
2. $10/month for a `backup service`_, also from Linode.com.
3. $17/year for our main domain, `openlp.org`_. See `Gandi.net's pricing page`_ for information on domain name costs.
4. $35/year for our developer domain, `openlp.io`_.


[ Image Credit: `Tip Jar at Open Bar by Dave Dugdale`_ ]

.. _Extra RAM for virtual machines: https://www.amazon.com/dp/B08H7T81YV?psc=1&ref=ppx_yo2ov_dt_b_product_details
.. _4K monitor for Hi DPI testing: https://www.amazon.com/gp/product/B08MXQ4NJ9/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1
.. _Refurbished Apple M1 Mac Mini: https://www.apple.com/shop/product/FGNR3LL/A/refurbished-mac-mini-apple-m1-chip-with-8%E2%80%91core-cpu-and-8%E2%80%91core-gpu
.. _Apple M2 Mac Mini: https://www.apple.com/shop/buy-mac/mac-mini/apple-m2-chip-with-8-core-cpu-and-10-core-gpu-256gb
.. _Raoul: https://forums.openlp.org/profile/raoul
.. _your Inbox: https://forums.openlp.org/messages/inbox
.. _forums: https://forums.openlp.org/
.. _come into IRC: https://kiwiirc.com/client/irc.libera.chat/?nick=guest?#openlp
.. _VPS from Linode.com: https://www.linode.com/pricing
.. _backup service: https://www.linode.com/backups
.. _Gandi.net's pricing page: https://www.gandi.net/domain/price/info
.. _openlp.org: https://openlp.org/
.. _openlp.io: https://openlp.io/
.. _Tip Jar at Open Bar by Dave Dugdale: https://www.flickr.com/photos/davedugdale/5025601209/

.. |ss| raw:: html

   <strike>

.. |se| raw:: html

   </strike>
